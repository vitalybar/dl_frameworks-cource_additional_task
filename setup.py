from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='XGBOOST model with optuna and MLFlow tracking in postgresql.',
    author='Vitaly Baranov',
    license='',
)
